import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import RecipeContainer from '../components/RecipeContainer'

const Recipes = () => (
  <Layout>
    <SEO title="recipes" />
    <h1>Recipes</h1>
    <Link to="/">Go back to the homepage</Link>
    
    <RecipeContainer />

  
  </Layout>
)

export default Recipes
