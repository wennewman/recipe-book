import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Some junk goes here to introduce the site</h1>
    <Link to="/recipes/">Page 2 has a list and some recipes</Link> <br />
  </Layout>
)
    
const LandingImage = () => (
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
  );

export default IndexPage
