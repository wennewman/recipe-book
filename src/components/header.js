import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"

const Header = ({ siteMetadata: { title, description } }) => (
  <header
    style={{
      background: `#38a21b`,
      marginBottom: `1.45rem`,
    }}
  >
    <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `1.45rem 1.0875rem`,
      }}
    >
      <h1 style={{ 
          margin: 0, 
          padding: `0px 0px 30px 0px` 
          }}
      >
        <Link
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,

          }}
        >
          {title}
        </Link>
      </h1>
        <div style={{ color: 'white' }}><p>{description}</p></div>
    </div>
  </header>
)

Header.propTypes = {
  siteMetadata: PropTypes.object
}

Header.defaultProps = {
  siteMetadata: {
    title: ``,
    description: ``
  }
}

export default Header
