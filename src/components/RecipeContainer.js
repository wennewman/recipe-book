import React, {useState} from "react"
import RecipeList from './RecipeList.js'
import { RecipeImage } from "../recipes/GenarrosRicottaDumplings.js";

const RecipeContainer = () => {
    // console.log(RecipeList); // If in doubt re triggering a re-render, uncomment.
    const [recipeIndex, setIndex] = useState(0);

    const onRecipeChange = (e) => {
        setIndex(e.currentTarget.value)
    }

    const recipe = RecipeList[recipeIndex]; 

    return (
        <div>
            <select value={recipeIndex} onChange={onRecipeChange}>
                { RecipeList.map(({ name }, i) => (<option value={i}>{name}</option>)) }
            </select>
            {
                recipe == null
                    ? <h1>Select a recipe, knobhead</h1>
                    : <>
                        <h2>{recipe.name}</h2>
                            { recipe != null && <recipe.Blurb/> }

                        <h4>INGREDIENTS</h4>
                            { recipe != null && <recipe.Ingredients /> }
                        <h4>METHOD</h4>
                        <ol>
                            {recipe != null && <recipe.Method />}
                        </ol>
                    </>
            }
        </div>
)}

export default RecipeContainer
