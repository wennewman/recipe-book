import React from "react"
import * as StaleCakeSlice from '../recipes/StaleCakeSlice.js'
import * as CurriedSausages from '../recipes/CurriedSausages.js'
import * as ThaiCoconutSoup from '../recipes/ThaiCoconutSoup.js'
import * as HungryBoyCake from '../recipes/HungryBoyCake'
import * as SuperSmoothie from '../recipes/SuperSmoothie'
import * as EasyChickenRice from '../recipes/EasyChickenRice'
import * as BubbleSlice from '../recipes/BubbleSlice'
import * as RalsSponge from '../recipes/RalsSponge'
import * as PumpkinSoup from '../recipes/PumpkinSoup'
import * as ChickenBroth from '../recipes/ChickenBroth.js'
import * as Anzacs from '../recipes/Anzacs.js'
import * as GenarrosRicottaDumplings from '../recipes/GenarrosRicottaDumplings.js'
import * as MushroomPastaSauce from '../recipes/MushroomPastaSauce.js'
import * as RaspberryCoconutSlice from '../recipes/RaspberryCoconutSlice.js'
import * as BananaDateCake from '../recipes/BananaDateCake.js'
import * as CupCakes from '../recipes/CupCakes.js'
import * as LemonRicottaCake from '../recipes/LemonRicottaCake.js'
import * as Cacik from '../recipes/Cacik.js'
import * as DevilsFoodCake from '../recipes/DevilsFoodCake.js'
import * as Scones from '../recipes/Scones.js'
import * as ChocCookies from '../recipes/ChocCookies.js'
import * as GingerFluff from '../recipes/GingerFluff.js'
import * as CoconutRoughBiscuits from '../recipes/CoconutRoughBiscuits.js'
import * as FingerSangas from '../recipes/FingerSangas.js'
import * as JellyCakes from '../recipes/JellyCakes.js'
import * as MexiMince from '../recipes/MexiMince.js'
import * as GingerShortbreadCreams from '../recipes/GingerShortbreadCreams.js'
import * as Hedgehog from '../recipes/Hedgehog.js'
import * as Truffles from '../recipes/Truffles.js'
import * as WalnutCake from '../recipes/WalnutCake.js'
import * as ShortbreadYoYos from '../recipes/ShortbreadYoYos.js'
import * as ChocolateCreamBiscuits from '../recipes/ChocolateCreamBiscuits.js'
import * as GingerBread from '../recipes/GingerBread.js'
import * as Lamingtons from '../recipes/Lamingtons.js'

const ListOptions = [
     {
        name: 'Lamingtons',
        categories: Lamingtons.categories,
        Blurb: () => <Lamingtons.Blurb/>,
        Ingredients: () => <Lamingtons.Ingredients />, 
        Method: () => <Lamingtons.Method />
    },
     {
        name: 'Ginger bread',
        categories: GingerBread.categories,
        Blurb: () => <GingerBread.Blurb/>,
        Ingredients: () => <GingerBread.Ingredients />, 
        Method: () => <GingerBread.Method />
    },
     {
        name: 'Chocolate cream biscuits',
        categories: ChocolateCreamBiscuits.categories,
        Blurb: () => <ChocolateCreamBiscuits.Blurb/>,
        Ingredients: () => <ChocolateCreamBiscuits.Ingredients />, 
        Method: () => <ChocolateCreamBiscuits.Method />
    },
     {
        name: 'Shortbread Yo Yos',
        categories: ShortbreadYoYos.categories,
        Blurb: () => <ShortbreadYoYos.Blurb/>,
        Ingredients: () => <ShortbreadYoYos.Ingredients />, 
        Method: () => <ShortbreadYoYos.Method />
    },
     {
        name: 'Walnut cake',
        categories: WalnutCake.categories,
        Blurb: () => <WalnutCake.Blurb/>,
        Ingredients: () => <WalnutCake.Ingredients />, 
        Method: () => <WalnutCake.Method />
    },
     {
        name: 'Truffles',
        categories: Truffles.categories,
        Blurb: () => <Truffles.Blurb/>,
        Ingredients: () => <Truffles.Ingredients />, 
        Method: () => <Truffles.Method />
    },
    {
        name: 'Hedgehog',
        categories: Hedgehog.categories,
        Blurb: () => <Hedgehog.Blurb/>,
        Ingredients: () => <Hedgehog.Ingredients />, 
        Method: () => <Hedgehog.Method />
    },
    {
        name: 'Ginger shortbread cream biscuits',
        categories: GingerShortbreadCreams.categories,
        Blurb: () => <GingerShortbreadCreams.Blurb/>,
        Ingredients: () => <GingerShortbreadCreams.Ingredients />, 
        Method: () => <GingerShortbreadCreams.Method />
    },
    {
        name: 'Mexi mince',
        categories: MexiMince.categories,
        Blurb: () => <MexiMince.Blurb/>,
        Ingredients: () => <MexiMince.Ingredients />, 
        Method: () => <MexiMince.Method />
    },
    {
        name: 'Jelly cakes',
        categories: JellyCakes.categories,
        Blurb: () => <JellyCakes.Blurb/>,
        Ingredients: () => <JellyCakes.Ingredients />, 
        Method: () => <JellyCakes.Method />

    },
    {   
        name: 'Salmon finger sangas',
        categories: FingerSangas.categories,
        Blurb: () => <FingerSangas.Blurb/>,
        Ingredients: () => <FingerSangas.Ingredients />, 
        Method: () => <FingerSangas.Method />
    }, 
    {  
        name: 'Coconut rough biscuits',
        categories: CoconutRoughBiscuits.categories,
        Blurb: () => <CoconutRoughBiscuits.Blurb/>,
        Ingredients: () => <CoconutRoughBiscuits.Ingredients />, 
        Method: () => <CoconutRoughBiscuits.Method />
    }, 
    { 
        name: 'Chocolate peanut butter cookies',
        categories: ChocCookies.categories,
        Blurb: () => <ChocCookies.Blurb/>,
        Ingredients: () => <ChocCookies.Ingredients />, 
        Method: () => <ChocCookies.Method />
    },    
    {  
        name: 'Scones',
        categories: Scones.categories,
        Blurb: () => <Scones.Blurb/>,
        Ingredients: () => <Scones.Ingredients />, 
        Method: () => <Scones.Method />
    },
    {  
        name: 'Devil\'s Food Cake',
        categories: DevilsFoodCake.categories,
        Blurb: () => <DevilsFoodCake.Blurb/>,
        Ingredients: () => <DevilsFoodCake.Ingredients />, 
        Method: () => <DevilsFoodCake.Method />
    },
    {
        name: 'Cacik',
        categories: Cacik.categories,
        Blurb: () => <Cacik.Blurb/>,
        Ingredients: () => <Cacik.Ingredients />, 
        Method: () => <Cacik.Method />
    },
    {
        name: 'Lemon ricotta cake',
        categories: LemonRicottaCake.categories,
        Blurb: () => <LemonRicottaCake.Blurb/>,
        Ingredients: () => <LemonRicottaCake.Ingredients />, 
        Method: () => <LemonRicottaCake.Method />
    },
    { 
        name: 'Cup cakes',
        categories: CupCakes.categories,
        Blurb: () => <CupCakes.Blurb/>,
        Ingredients: () => <CupCakes.Ingredients />, 
        Method: () => <CupCakes.Method />
    },
    { 
        name: 'Stale cake slice',
        categories: StaleCakeSlice.categories,
        Blurb: () => <StaleCakeSlice.Blurb/>,
        Ingredients: () => <StaleCakeSlice.Ingredients />, 
        Method: () => <StaleCakeSlice.Method />
    },
    { 
        name: 'Curried sausages',
        categories: CurriedSausages.categories,
        Blurb: () => <CurriedSausages.Blurb/>,
        Ingredients: () => <CurriedSausages.Ingredients />, 
        Method: () => <CurriedSausages.Method />
    },
    { 
        name: 'Thai coconut soup',
        categories: ThaiCoconutSoup.categories,
        Blurb: () => <ThaiCoconutSoup.Blurb/>,
        Ingredients: () => <ThaiCoconutSoup.Ingredients />, 
        Method: () => <ThaiCoconutSoup.Method />
    },
    {
        name: 'Hungry boy cake',
        categories: HungryBoyCake.categories,
        Blurb: () => <HungryBoyCake.Blurb/>,
        Ingredients: () => <HungryBoyCake.Ingredients/>,
        Method: () => <HungryBoyCake.Method/>
    },
    {
        name: 'Super smoothy',
        categories: SuperSmoothie.categories,
        Blurb: () => <SuperSmoothie.Blurb/>,
        Ingredients: () => <SuperSmoothie.Ingredients/>,
        Method: () => <SuperSmoothie.Method/>
    },
    {
        name: 'Easy chicken rice',
        categories: EasyChickenRice.categories,
        Blurb: () => <EasyChickenRice.Blurb/>,
        Ingredients: () => <EasyChickenRice.Ingredients/>,
        Method: () => <EasyChickenRice.Method/>
    },
    {
        name: 'Bubble slice',
        categories: BubbleSlice.categories,
        Blurb: () => <BubbleSlice.Blurb/>,
        Ingredients: () => <BubbleSlice.Ingredients/>,
        Method: () => <BubbleSlice.Method/>
    },
    {
        name: 'Ral\'s sponge',
        categories: RalsSponge.categories,
        Blurb: () => <RalsSponge.Blurb/>, 
        Ingredients: () => <RalsSponge.Ingredients/>,
        Method: () => <RalsSponge.Method/>
    }, 
    {
        name: 'Pumpkin soup',
        categories: PumpkinSoup.categories,
        Blurb: () => <PumpkinSoup.Blurb/>, 
        Ingredients: () => <PumpkinSoup.Ingredients/>,
        Method: () => <PumpkinSoup.Method/>
    },
    {
        name: 'Chicken stock',
        categories: ChickenBroth.categories,
        Blurb: () => <ChickenBroth.Blurb/>, 
        Ingredients: () => <ChickenBroth.Ingredients/>,
        Method: () => <ChickenBroth.Method/>
    },
    {
        name: 'Anzac biscuits',
        categories: Anzacs.categories,
        Blurb: () => <Anzacs.Blurb/>, 
        Ingredients: () => <Anzacs.Ingredients/>,
        Method: () => <Anzacs.Method/>
    },
    {
        name: 'Genarro\'s ricotta dumplings',
        categories: GenarrosRicottaDumplings.categories,
        Blurb: () => <GenarrosRicottaDumplings.Blurb/>, 
        RecipeImage: () => <GenarrosRicottaDumplings.RecipeImage/>,
        Ingredients: () => <GenarrosRicottaDumplings.Ingredients/>,
        Method: () => <GenarrosRicottaDumplings.Method/>
    },
    {
        name: 'Mushroom pasta sauce',
        categories: MushroomPastaSauce.categories,
        Blurb: () => <MushroomPastaSauce.Blurb/>, 
        Ingredients: () => <MushroomPastaSauce.Ingredients/>,
        Method: () => <MushroomPastaSauce.Method/>
    },
    {
        name: 'Georgie\'s raspberry coconult slice',
        categories: RaspberryCoconutSlice.categories,
        Blurb: () => <RaspberryCoconutSlice.Blurb/>,
        Ingredients: () => <RaspberryCoconutSlice.Ingredients/>,
        Method: () => <RaspberryCoconutSlice.Method/>
    },
    {
        name: 'Banana date cake',
        categories: BananaDateCake.categories,
        Blurb: () => <BananaDateCake.Blurb/>,
        Ingredients: () => <BananaDateCake.Ingredients/>,
        Method: () => <BananaDateCake.Method/>
    },
    {
        name: 'Ginger fluff sponge',
        categories: GingerFluff.categories,
        Blurb: () => <GingerFluff.Blurb/>,
        Ingredients: () => <GingerFluff.Ingredients/>,
        Method: () => <GingerFluff.Method/>
    }
    
]

export default ListOptions;
