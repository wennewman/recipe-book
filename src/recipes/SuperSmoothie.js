import React from 'react'

const categories = ['snack', 'breakfast', 'gluten free'];

const Blurb = () => (
    <>
        <p>Credit Leonie Henderson for this one; not sure where she got it from but as far as healthy smoothies go - this is a palatable cracker.</p>
    </>
)

const Ingredients = () => (

    <>
        <p>3 cups spinach</p>
        <p>1 bunch kale</p>
        <p>2 small carrots</p>
        <p>1 tomato</p>
        <p>1 avocado</p>
        <p>1 banana</p>
        <p>1 cup blueberries</p>
        <p>½ cup linseeds</p>
        <p>½ cup pumpkin seeds</p>
        <p>½ red pepper</p>
        <p>1tbsp honey (optional)</p>
        <p>1 litre of cold water</p>
        <p>1 cup ice (optional)</p>
    </>
);

const Method = () => (
    <>
        <li>Blend all ingredients flat out until smooth</li>
        <li>Feel your belly smile as you drink it</li>
    </>
);

export {Ingredients, Method, categories, Blurb};