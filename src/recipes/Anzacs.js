import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
<>
    <p>Mum's recipe; it is different every time I make them.</p>
    <p>I use the base of this to make loads of cookies; add peanut butter and an egg to make some delightful gooeyness.</p>
</>
);

const Ingredients = () => (
<>
    <p>1 cup plain flour</p>
    <p>1 cup sugar</p>
    <p>1 cup oats</p>
    <p>1 cup dessicated coconut</p>
    <p>2 tbsp golden syrup</p>
    <p>1 tsp bi carb soda</p>
    <p>2 tbsp water</p>
    <p>125g butter</p>
</>
);

const Method = () => (
    <>
        <li>Set oven to moderate - 180ºc</li>
        <li>Melt butter, water and gliden syrup in a saucepan to a simmer. Add soda and stir so it froths</li>
        <li>While butter is melting, mix dry ingredients in a large bowl and set aside</li>
        <li>Add butter to dry ingredients and mix to a stiff biscuit dough</li>
        <li>Rlil out into balls and place on an oven tray approximately 3cm apart</li>
        <li>Bake until golden brown; approx 15-20 minutes</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
