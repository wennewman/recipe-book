import React from 'react'

const categories = ['snack'];

const Blurb = () => (
<>
    <p>Yummy Turkish yoghurt and cucumber dip</p>
</>
);

const Ingredients = () => (
<>
    <p>1 small Lebanese cucumber</p>
    <p>3 crushed garlic cloves</p>
    <p>Salt and ground black pepper</p>
    <p>300ml Greek yoghurt</p>
    <p>2tbsp chopped fresh mint</p>
</>
);

const Method = () => (
    <>
        <li>Wash then finely dice cucumber, leaving the skin on</li>
        <li>Place in a colander and cover with salt to drain over the sink for 30 minutes</li>
        <li>Rinse off thoroughly and pat dry with paper towel</li>
        <li>Mix minced garlic with yoghurt, salt and pepper, chopped mint and cucumber</li>
        <li>Serve with Turkish bread, toasted pita bread or veggie sticks. Super yum</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
