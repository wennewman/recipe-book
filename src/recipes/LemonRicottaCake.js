import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
<>
    <p>Totes made this one up. Rad</p>
    <p>If you dont have any almond meal, use another third cup of SR flour.  I make this sometimes with the leftover egg whites from the ricotta gnocchi but the recipe stands up fine without egg whites.  When you use a 375g ricotta tub for the gnocchi, this is just the leftovers.</p>
</>
);

const Ingredients = () => (
<>
    <p>125g softened butter</p>
    <p>150g ricotta cheese (full fat is best)</p>
    <p>1 cup sugar</p>
    <p>2 eggs</p>
    <p>Zest and juice of a lemon</p>
    <p>1 cup SR flour</p>
    <p>1/3 cup ground almonds</p>
    <p>3 egg whites</p>

</>
);

const Method = () => (
    <>
        <li>Grease and line a deep loaf or cake tin.  Set the oven to 180ºc</li>
        <li>Beat egg whites until peaks form</li>
        <li>In separate bowl, cream butter, ricotta and sugar with electric beater until pale and lighter in colour</li>
        <li>Add eggs, one a time until mixed through</li>
        <li>Fold through almond meal and lemon until combined</li>
        <li>Fold through flour until just combined</li>
        <li>If using egg whites, fold these through last</li>
        <li>Bake in a moderate oven for approximately 45 minutes.  You will smell the cake once it is cooked; stab with a knife which should come out clean when cooked</li>
    </>
);

export { Ingredients, Method, categories, Blurb };

