import React from 'react'

const categories = ['dinner', 'gluten free'];

const Blurb = () => (
<>
<p>This recipe got my rice averse small person eating rice, so I guess that means its a good family dinner.  Credit, some magazine which I cut out from many moons ago, and myself for making it better.</p>
<p>You can cook this in a pan on the cook top or bake in the oven.  You could even do in the pressure cooker - probably take about 10 minutes once it is up to pressure (I've not tried that yet)</p>
</>
);

const Ingredients = () => (
<>
<p>2 tomatoes</p>
<p>1 red onion</p>
<p>1 garlic clove</p>
<p>2 tbsp olive oil</p>
<p>1kg chicken pieces with skin on</p>
<p>1 cup rice</p>
<p>2 cups of any stock (or water and a stock cube)</p>
<p>2 carrots, finely chopped</p>
<p>1 cup frozen peas</p>
<p>1 green peper, finely chopped</p>
<p>1 corn cob with the kernels sliced off (or small tin of drained corn)</p>
<p>1 tsp salt</p>
<p>1 tsp ground pepper - black or white is good</p>
</>
);

const Method = () => (
    <>
    <li>Turn on the oven to 180ºc</li>
    <li>Blend the tomato, garlic and onion in a food processor or blender</li>
    <li>Heat the oil in large oven proof pan; you don't want it too hot.  Brown the chiken pieces - 2-3 minutes should do it. Take them out and put on a plate for now</li>
    <li>Add rice to the pan and cook for 2-3 minutes, stirring and coating with the chicken juice and residual oil in the pan.  The rice will go a little brown whci is what you want.  Pour in onion and tomato puree and cook for 2-3 minutes until the juice is cooked out a little</li>
    <li>Stir in stock and veges and bring to the boil.  Place the chicken pieces back in - nesting them down into the rice</li>
    <li>Put the lid on the pan and bake for 25 minutes, or until chicken is cooked through</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
