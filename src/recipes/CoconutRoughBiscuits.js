import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
<>
    <p>Totes made this up.  Rad.</p>
</>
);

const Ingredients = () => (
    <>
        <p>1 cup sugar (any sugar is good)</p>
        <p>1 cup coconut</p>
        <p>1½ cups plain flour</p>
        <p>2 heaped tbsp cocoa</p>
        <p>1 egg</p>
        <p>Vanilla</p>
        <p>140g butter</p>
        <p>150g of dark chocolate</p>
        <p>2 tbsp coconut oil</p>
    </>
);

const Method = () => (
    <>
        <li>Set your oven at 180ºc</li>
        <li>Cover your bikky tray with baking paper or grease with the butter wrapping paper</li>
        <li>Cream butter, sugar and vanilla in bowl with electric mixer</li>
        <li>Sift flour and cocoa</li>
        <li>Add egg to butter and mix until combined</li>
        <li>Add flour and mix until combined</li>
        <li>Roll into balls and place on your prepared tray</li>
        <li>Press down on the top with a fork and bake for 15-20 minutes</li>
        <li><strong>For the topping:</strong>Melt the chocolate with the coconut oil</li>
        <li>Once the biscuits are cool enough to handle, dip top half in melted chocolate then dip straight into coconut</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
