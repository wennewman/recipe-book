import React from 'react'

const categories = ['afternoon tea', 'no bake'];

const Blurb = () => (
<>
    <p>This is the thing with the condensed milk</p>
</>
);

const Ingredients = () => (
<>
    <p>1 tin of condensed milk</p>
    <p>½ lb crushed plain biscuits</p>
    <p>1 tbsp cocoa</p>
    <p>½ cup coconut</p>
    <p>1 tsp vanilla extract</p>
</>
);

const Method = () => (
    <>
        <li>Mix all the ingredients except coconut until it is all combined</li>
        <li>Roll tablespoons of mixture into balls and roll in coconut</li>
        <li>Put in an air tight container in the fridge</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
