import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
<>
    <p>This is Georgie McLeod's recipe.  Yvonne wrote this out for me; I have never made it but she made it all the time. Another fav. I'm not sure if the cups are old school tea cups or actually metric baking measuring cups.  Good luck! At some point I'll give it a burl and update.</p>
</>
);

const Ingredients = () => (
<>
    <p>2 cups flour</p>
    <p>½ cup sugar</p>
    <p>2 eggs</p>
    <p>125g butter</p>
    <p>2 tbsp sugar extra</p>
    <p>2 cups coconut</p>
    <p>¼ cup raspberry or other jam</p>
</>
);

const Method = () => (
    <>
        <li>Set oven to 180ºc moderate</li>
        <li>Grease and line square or rectangular slice tray</li>
        <li>Mix flour and sugar together and rub in chopped butter. Add egg and mix to dough; do not over work the dough</li>
        <li>Press and smooth out pastry base into prepared slice tray then spread with your fav berry (or apricot jam)</li>
        <li>Mix coconut, extra sugar and lightly beaten egg until all combined</li>
        <li>Spread over pastry base</li>
        <li>Bake in moderate oven for approximately 30 minutes, or until lightly browned on the top and the pastry base is cooked through</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
