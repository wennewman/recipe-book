import React from 'react'

const categories = ['dinner', 'gluten free'];

const Blurb = () => (
<>
    <p>I made this up.  I had no el paso spice mix and love corn and red pepper together so here tis. When I use cumin and coriander I dry toast the seeds myself in a dry frying pan and grind up in my portal and vessel.  You can just use it already ground out of a packet if you cbf with that fuss.</p>
</>
);

const Ingredients = () => (
<>
    <p>500g beef mince </p>
    <p>½ diced red pepper</p>
    <p>1 - 2 corn cobs (to your taste)</p>
    <p>2-3 chopped garlic cloves</p>
    <p>1 tbsp coriander seeds, toasted and ground (2 tsps if using ground)</p>
    <p>1 tbsp cumin seeds, toasted and ground (2 tsps if using ground)</p>
    <p>1 tsp fennel seeds, toasted and ground (1 tsp if using ground)</p>
    <p>1 tsp garlic powder</p>
    <p>1 tbsp smokey paprika</p>
    <p>1 tsp celery salt</p>
    <p>1 tsp onion powder</p>
    <p>t tsp salt</p>
    <p>1 tsp chilli flakes (or a chopped whole chilli)</p>
    <p>1 chopped onion</p>
    <p>1 tin of drained kidney beans (or a cup of your own home prepared beans</p>
    <p>2 tbsp tomato paste</p>
    <p>2 tbsps Olive oil</p>
    <p>1 square of 70% (or higher) dark chocolate</p>
</>
);

const Method = () => (
    <>
        <li>Place the seeds in a dry pan and fry on medium heat for 2 minutes or until aromatic.  Pay attention as they can burn easily</li>
        <li>Once smelling nice, put the seeds in your pestle and mortar or spice grinder and grind to powder.</li>
        <li>Crush or finely chop garlic and chilli if using, and finely chop onion and set aside</li>
        <li>Slice the corn off the cobs, chop the red pepper into corn cob sized cubes and set aside</li>
        <li>In a large heavy based pan, add all of the dry spices and combine.  Make sure you mix the garlic powder in well as it can clump together once it heats up</li>
        <li>Add the oil and mix in the spices and then turn on the heat</li>
        <li>Add onions, chilli, garlic, red pepper and corn and fry on medium heat until onions are transparent</li>
        <li>Push the veggies to the edge of the pan and turn up the heat a little.  Add the mince and brown for 5 minutes</li>
        <li>Mix the veggies back into the mince mixture and cook for another few minutes</li>
        <li>Add the tomato paste, drained beans and a little water or stock if too dry</li>
        <li>Cook for a few minutes and add the chocolate</li>
        <li>The mix should be a little dry for tacos, tortillas or a mexi bowl</li>
        <li>Serve with guac and salad, on a baked spud with cheese or whatever else floats your mexi boat</li>

    </>
);

export { Ingredients, Method, categories, Blurb };
