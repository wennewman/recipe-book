import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
<>
    <p>More cake!</p>
</>
);

const Ingredients = () => (
    <>
        <p>1½ cups SR flour</p>
        <p>1 cup castor sugar</p>
        <p>½ cup whole milk</p>
        <p>2 eggs</p>
        <p>1 tsp vanilla essence</p>
        <p>125g melted butter</p>
    </>
);

const Method = () => (
    <>
        <li>Set moderate oven (180ºc)</li>
        <li>Set cup cake papers on a tray or into muffin tin (helps hold them in shape)</li>
        <li>Mix Flour and sugar in mixing bowl</li>
        <li>In a separate bowl lighly beat eggs with a fork and add milk and combine</li>
        <li>Add milk mixture to flour and mix until just combined.  Add melted butter and mix until just combined</li>
        <li>Spoon into cake papers and bake in moderate oven for approximately 20 minutes or until knife comes out clean when stabbed</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
