import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
<>
    <p>Just bloody delicious if you like ginger.  Turns out I'm quite fond of Ginger.  This is not biscuits to make for christmas, it is a really moist delicious cake.</p>
</>
);

const Ingredients = () => (
    <>
        <p>½ cup butter</p>
        <p>1 cup sugar</p>
        <p>3 eggs</p>
        <p>1 cup milk</p>
        <p>½ cup treacle (or golden syrup but treacle is better)</p>
        <p>1 tbsp ginger</p>
        <p>½ tsp bi carb soda</p>
        <p>2 cups flour</p>
        <p>1 tbsp boiling water</p>
    </>
);

const Method = () => (
    <>
        <li>Set oven to 180ºc</li>
        <li>Grease and line lamington tin</li>
        <li>Boil the kettle and dissolve the soda in hot water</li>
        <li>Cream butter and sugar with electric mixer</li>
        <li>Beat in eggs, one at a time</li>
        <li>Add milk then sifted ginger and flour alternately, mixing after each addition until all combined</li>
        <li>Add treacle and soda</li>
        <li>Pour into prepared tin and bake for approximately 1 hour or until knife comes out clean</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
