import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
<>
    <p>These are mum's chocolate forcer biscuits.  She made them only occasionally as a treat and we totally devoured them</p>
</>
);

const Ingredients = () => (
<>
    <p>2 cups flour</p>
    <p>½ cup butter</p>
    <p>1 cup sugar</p>
    <p>½ tsp baking powder</p>
    <p>½ tsp bi carb soda</p>
    <p>½ tsp cinnamon</p>
    <p>3 tbsp cocoa</p>
    <p>1 egg</p>
</>
);

const Method = () => (
    <>
        <li>Set oven to moderate 180ºc</li>
        <li>Grease baking sheet or cover with baking paper</li>
        <li>Cream butter and sugar</li>
        <li>Beat in egg</li>
        <li>Add sifted dry ingredients and beat with flat beater until forms a dough</li>
        <li>Roll out and cut with cookie cutter or push into biscuit forcer</li>
        <li>Bake in moderate oven for 12-15 minutes until cooked</li>
        <li>Cool on cooling rack and join with chocolate icing once cooled</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
