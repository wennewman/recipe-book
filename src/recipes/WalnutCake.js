import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
<>
    <p>I think this is nana's recipe; I can't remember having made this one.</p>
</>
);

const Ingredients = () => (
<>
    <p>60g butter</p>
    <p>½ cup sugar</p>
    <p>2 tbsp milk</p>
    <p>1¼ cup SR flour</p>
    <p>1 tbsp cocoa</p>
    <p>100g chopped walnuts</p>
    <p>½ tsp mixed spice</p>
    <p>1 egg</p>
</>
);

const Method = () => (
    <>
        <li>Set oven to 180ºc</li>
        <li>Grease and line 20 cm cake or loaf tin</li>
        <li>Cream butter and sugar in small bowl with electric beater</li>
        <li>Beat in egg</li>
        <li>Mix in half the sifted dry ingredients with half the milk</li>
        <li>Mix in remaining dry ingredients then remaining milk</li>
        <li>Pour into prepared tin and bake 20 to 40 minutes or until knife comes out clean</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
