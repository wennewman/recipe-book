import React from "react"

const categories = ['baking', 'snack','afternoon tea'];

const Blurb = () => (
    <>
    <p>Credit Chrissy Palmer for this one.  Probably my fav fruit cake, given I'm not a huge fruit cake lover.</p>
    </>
)

const Ingredients = () => (
    <>
        <p>2½ cups plain flour</p>
        <p>1½ cups sugar</p>
        <p>3 eggs</p>
        <p>250g butter</p>
        <p>450g sultanas</p>
        <p>1tsp vanilla essence</p>
        <p>½tsp almond essence</p>
        <p>1½tsps baking powder</p>
        <p>Optional: ½ cup chopped almonds or other nuts</p>
    </>
);

const Method = () => (
  <>
        <li>Set oven at 180ºc.  Line and grease a 22cm square high sided cake tin</li>
        <li>In a saucepan, cover sultanas with water.  Bring to boil and simmer for 5 minutes.</li>
        <li>Remove sultanas from heat, add chopped butter and stir.  Set aside for butter to melt and mixture to cool</li>
        <li>Beat eggs and sugar in mixer until thick and creamy</li>
        <li>Mix melted butter and sultanas.  Add nuts if using. Fold through egg mixture with almond and vanilla essences</li>
        <li>Sift baking powder and flour twice and then add to mixture</li>
        <li>Bake for 1¼ hours in moderate oven or until knife comes out clean; time can vary depending on your oven</li>
  </>
);

export {Ingredients, Method, categories, Blurb};
