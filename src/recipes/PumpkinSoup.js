import React from 'react'

const categories = ['dinner', 'gluten free'];

const Blurb = () => (
<>
    <p>This is mint if you have some home made chicken stock and can be arsed roasting the pumpkin first.  It is totally not necessary to roast the pumpkin first if you short of time.</p>
    <p>As a variation, I coat the pumpkin with green or red Thai paste before roasting, and fry some more aromats with the onion in the soup pot (coriander root, garlic, ginger)</p>
</>
);

const Ingredients = () => (
<>
    <p>Half a medium sized pumpkin, chopped into roast size pieces</p>
    <p>Handfull of sage leaves</p>
    <p>4 cloves of garlic</p>
    <p>Salt and pepper</p>
    <p>2 chopped potatoes</p>
    <p>1 red onion chopped into quarters</p>
    <p>1 onion</p>
    <p>1 litre of chicken stock</p>
    <p>Olive oil</p>
</>
);

const Method = () => (
    <>
        <li>Set oven at 200ºc</li>
        <li>Place pumpkin, whole garlic cloves, sage and quartered red onions onto oven tray or roasting dish and cover with salt, pepper and olive oil.  Roast until pumpkin is soft and cooked through; approximately 40 minutes. (Hack: Cut pumpkin pieces smaller if you are short of time or impatient)</li>
        <li>While pumpkin is cooking, chop onion and a few additional sage leaves and fry in large soup pot with more olive oil, salt and pepper.  Cook through until onion is transparent</li>
        <li>Add peeled and chopped potato and stir, cooking for 5 minutes</li>
        <li>Add chicken stock and bring up to simmer</li>
        <li>Once pumpkin is cooked, add to soup pot and simmer until spuds are soft</li>
        <li>Deglaze the roasting pan to get all the good caramelised flavour off (you can spoon a little of the stock out of the pot onto your roasting pan to get it off)</li>
        <li>Tip the deglazed juice out back into your soup pot</li>
        <li>Once all the veggies are cooked, blend in blender or using stick blender until smooth</li>
        <li>Serve with a dollop of sour cream</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
