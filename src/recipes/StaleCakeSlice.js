import React from "react"

const categories = ['snack', 'dessert', 'afternoon tea'];

const Blurb = () => (
    <>
        <p>This is one of my mum's recipes. Probably one of my most fav baked things of all time; up there with a good eccles cake if you like that kind of thing.</p>
    </>
);

const Ingredients = () => (
    <>
        <p><strong>Filling:</strong></p>
        <p>½lb stale cake crumbs</p>
        <p>½lb currants</p>
        <p>1½ tbsp butter</p>
        <p>1tsp cinnamon</p>
        <p>6tbsp milk</p>
        <p><strong>Pastry:</strong></p>
        <p>5oz butter</p>
        <p>1½ cups four</p>
        <p>1 tsp baking powder</p>
    </>
);

const Method = () => (
    <>
        <p>Set oven to 180ºc. Line and grease a square or rectangle lamington/slice tin</p>
        <p><i>For the pastry</i></p>
        <li>Sift baking powder and flour into large</li>
        <li>Chop butter into small pieces into the flour</li>
        <li>Mix together into a firm dough, adding a little milk if needed</li>
        <li>Press pastry (or roll - whichever is easier) into a square 10" lamington tin</li>
        <li><strong>For the filling</strong>:Mix all ingredients in saucepan over low heat and combine well</li>
        <li>Pour into pastry case</li>
        <li>Bake in moderate oven for approximately 30 minutes or until the top is soft to touch but dry in the centre</li>
    </>
);

export { Ingredients, Method, categories, Blurb };