import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
<>
    <p>Seems I've a lot of afternoon tea recipes.  This is a completely made up recipe using remains of eggs from ricotta gnocchi. I've not made it for a long time so trusting I've nailed the details here.</p>
</>
);

const Ingredients = () => (
<>
    <p>3 egg whites</p>
    <p>125g butter</p>
    <p>1tbs golden syrup</p>
    <p>½ cup brown or raw sugar</p>
    <p>1 mashed banana</p>
    <p>½ cup chopped dates</p>
    <p>1 cup SR flour</p>
    <p>½ cup almond meal</p>
    <p>1tsp vanilla essence</p>
    <p>½tsp bi carb soda</p>
    <p>¼ cup water</p>
</>
);

const Method = () => (
    <>
        <li>Set oven to 180ºc</li>
        <li>Grease and line largish high sided cake tin</li>
        <li>Beat egg whites until stiff peaks</li>
        <li>Put the chopped dates and golden syrup into microwave dish and cover with water.  Microwave for approximately 3 minutes or until dates turn to soft mush when mashed with a fork</li>
        <li>In separate bowl, beat butter, sugar and vanilla until creamy</li>
        <li>While the dates are hot, add the soda and stir so it floofts up, then stir in the mashed banana and almond meal(this colis the mix too!)</li>
        <li>Fold the date mixture through the creamed butter and sugar</li>
        <li>Fold in flour and then egg whites</li>
        <li>Bake in oven for approximately 50 minutes.  You should smell the cake once it is cooked - when pierced with a knife, the knife should come out clean</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
