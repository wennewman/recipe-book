import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
    <>
        <p>Totes made this up</p>
    </>
);

const Ingredients = () => (
    <>
        <p>1½ cups plain flour</p>
        <p>½ cup almond meal</p>
        <p>125g butter</p>
        <p>2 tbsp peanut butter</p>
        <p>¼ cup cocoa</p>
        <p>2 tbsp water</p>
        <p>2 tbsp golden syrup</p>
        <p>1 tsp bi carb soda</p>
        <p>1 egg</p>
    </>
);

const Method = () => (
    <>
        <li>Set oven to moderate 180ºc</li>
        <li>Mix dry ingredients in a large mixing bowl</li>
        <li>Melt butter, peanut butter, golden syrup and water in saucepan.  Simmer for 1 minute</li>
        <li>Remove from heat and add soda, stirring so it floofts</li>
        <li>Poor mixture over dry ingredients and stir to cool the mixture down</li>
        <li>Add the lightly beaten egg and mix until smooth bikky dough</li>
        <li>Roll into balls and place on baking sheet.  Press down on top with a fork</li>
        <li>Bake in oven for approximately 15 minutes - less if you like them chewie; more if you like them crunchy</li>
        <li>If you are feeling fancy, you can dunk one side of each biscuit in melted chocolate</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
