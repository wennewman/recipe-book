import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
    <>
        <p>This recipe is from one of Yvonne's recipe cards from 70s. It is quite delicious.  You could slice each cake in half again to make a four layered gateau and fill it with GANACHE... much excesses</p>
    </>
);

const Ingredients = () => (
    <> 
            <h4>Cake ingredients</h4>
            <p>2¼ cups SR flour</p>
            <p>1 3/4 cups castor sugar</p>
            <p>1 tsp bi carb soda</p>
            <p>2/3 cup cocoa</p>
            <p>155g butter</p>
            <p>1 cup water</p>
            <p>1 tsp vanilla essence</p>
            <p>3 eggs</p>
            <h4>Icing ingredients</h4>
            <p>45g chocolate</p>
            <p>1tbsp oil or butter</p>
            <p>2tbsp water</p>
            <p>1¼ cup icing sugar</p>
    </>
);

const Method = () => (
    <>
        <li>Set oven at 180ºc</li>
        <li>Grease and line two deep 20cm cake tins</li>
        <li>Sift dry ingredients into mixer bowl</li>
        <li>Add softeneed butter, water and vanilla.  Beat on medium speed for 3 minutes</li>
        <li>Add eggs and increase speed.  Beat for further 3 minutes - mixture will lighten in colour</li>
        <li>Pour into prepared cake tins and bake for 35-40 minutes</li>
        <li><strong>For the icing</strong>: melt chocolate, oil and water in microwave dish</li>
        <li>Gradually whisk in icing sugar until combined and shiny</li>
        <li>Ice the cooled cake and fill with whipped cream</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
