import React from 'react'

const categories = ['essentials', 'gluten free'];

const Blurb = () => (
<>
<p>I'm pretty excited about this now as I have refined my flavour and it sets to jelly.  It can be used for all kinds of good.</p>
<p></p>
</>
);

const Ingredients = () => (
<>
    <p>2kg chicken bones (I use wings, frames, necks - actually anything.  You can also use a whole chook)</p>
    <p>bunch of parsley - curly or flat</p>
    <p>2 large carrots, washed and chopped into 2-3cm chunks</p>
    <p>2 large onions, peeled and chopped roughly</p>
    <p>1 leek (optional), roots removed and well washed, chopped the size of onions and carrots</p>
    <p>4 sticks of celery roughly chopped</p>
    <p>1 tbsp black peppercorns</p>
    <p>1 tbsp salt</p>
    <p>3 bay leaves</p>
    <p>2 sprigs of thyme</p>
    <p>Parmesan cheese rind (I throw these in the freezer once I finish the cheese block to use for stock) (optional)</p>
    <p>1 punnet of mushrooms, skins wiped(optional)</p>
    <p>3 garlic cloves</p>
    <p>1 slice of lemon rind</p>
    <p>6 litres cold water</p>
</>
);

const Method = () => (
    <>
        <li>Place the chicken into a large (10l) stock pot with the water and bring to boil.</li>
        <li>Skim the dirty foam from the top as it heats up.</li>
        <li>Grab a couple of big steel cooking bowls to help with prep - one for the chopped veggies; one for the veggie scraps.</li>
        <li>While the water is heating up, chop the veggies into </li>
        <li>Peel and chop the carrots into </li>
        <li>Put all the ingredients into your pressure cooker and fill up to 3 inches from the top</li>
        <li>Bring the pot up to pressure and cook on medium pressure for 1 hour</li>
        <li>Strain through a fine sieve and bottle while boiling to sterilise the bottles (I turn them upside down to also cook any residual germs in the jar/bottle lid)</li>
    </>
);

export { Ingredients, Method, categories, Blurb };

