import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
    <>
        <p>Mum defines this as a one egg sandwich but declares it as good for lamington sponge; so here goes.  I have never made this.</p>
    </>
);

const Ingredients = () => (
    <>
        <p>1 cup sugar</p>
        <p>1 tbsp butter</p>
        <p>1 egg</p>
        <p>1 cup milk</p>
        <p>1 tsp bi carb soda</p>
        <p>2 cups flour</p>
        <p>2 tsp cream of tartar</p>
        <p>¼ tsp salt</p>
        <p>1 tsp vanilla extract</p>
    </>
);

const Method = () => (
    <>
        <li>Set oven to 180ºc</li>
        <li>Grease and line lamington tin</li>
        <li>Melt butter and add sugar, egg and milk and beat</li>
        <li>Add sifted dry ingredients and mix well</li>
        <li>Pour into prepared tin and cook for approximately 20-30 minutes or until knife comes out clean</li>
        <li>Turn out on cooling rack and once completely cold, carefully cut into squares and place in the freezer</li>
        <li><strong>For the icing</strong>:Melt ½ teaspoon butter into 5 tablespoons boiling water until smooth.  Beat in 3 tablespoons cocoa and 3 cups icing sugar until smooth</li>
        <li>Once cake is almost frozen, dip into chocolate icing mixture then roll straight into coconut</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
