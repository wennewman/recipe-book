import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
<>
    <p>I just fail every time I make this.  I think it worked once; all the sponge planets aligned and I still have no idea what I did differently.  Serioulsy when it works though, this is my most fav cake of all time.</p>
</>
);

const Ingredients = () => (
<>
    <p>4 small eggs at room temperature</p>
    <p>3/4 cup castor sugar</p>
    <p>½ cup cornflour</p>
    <p>1 tbsp flour</p>
    <p>1 tsp cocoa</p>
    <p>1 tsp ground ginger</p>
    <p>1 tsp cream of tartar</p>
    <p>½ tsp bi-carb soda</p>
    <p>½ tbsp warmed golden syrup</p>
</>
);

const Method = () => (
    <>
    <li>Beat eggs and sugar together until thick and creamy - about 10 minutes (I usually put the timer on to mitigate stronking and time voids)</li>
    <li>While the eggs are beating, sift all of the dry ingredients together into a separate bowl and set aside.</li>
    <li>Measure out the golden syrup into a microwave safe something, and warm for a couple of seconds in the microwave and set aside</li>
    <li>Once the eggs and sugar are done, add the flour (I sift again into the eggs at this point) and the golden syrup</li>
    <li>Carefully fold the flour and syrup through the egg mixture - do not beat or mix - gentle folding so you dont flatten the eggs</li>
    <li>Pour into sponge tin - 2 sandwich tins or one bigger one (again I'm no sponge chef - I wing it each time - use what you know works</li>
    <li>Bake in moderate oven for about 30 minutes but start checking about 25 minutes in.</li>
    <li>For my twist at the end - hang the tin upside down until completely cooled</li>
    <li>I fill with whipped cream with a hint of vanilla added and ice with some chocolate icing</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
