import React from "react"
const categories = ['brunch', 'soup','lunch', 'dinner'];

const Blurb = () => (
    <>
    <p>Where to begin.  Credit Liz Baulch for this one.  Lizzy and I met in Israel on a Moshav, sharing a house with a Thai family, where we learned (well where Liz learned) to cook amazing Thai food.  This recipe is gold.  I havent made it ever - I was always the Pestle and Mortar bitch trying to stuff an enormous amount of herbs and aromats into a small hole.  Manually it takes a lot of arm power to get it all into a smulch, but it is so very worth it. </p>
    <p>I could go on and tell the story about the stolen turkey that we butchered in pitch black thinking we got away with it until we awoke to daylight and a scene from dexter with feathers++ on the dirt patio, but I will spare you.  </p>
    </>
);

const Ingredients = () => (
    <>
        <p>1 large onion</p>
        <p>2 red shallots</p>
        <p>1 bunch of coriander including roots</p>
        <p>1 bunch of flat parsley</p>
        <p>1 bunch of fresh dill</p>
        <p>1 head of garlic</p>
        <p>4cm square piece of peeled ginger</p>
        <p>1 x 425g tin of pilchards in brine</p>
        <p>2 red chillies</p>
        <p>100g drained green peppercorns </p>
        <p>juice and zest of 1 lime</p>
        <p>¼ cup soy sauce</p>
        <p>2tbs fish sauce</p>
        <p>2 x 420g cans of coconut milk</p>
        <p>500g chicken fillets</p>
        <p>200g rice noodles</p>
        <p>2 lightly beaten eggs</p>
        <p>300g snake beans</p>
        <p>1 head of chinese broccoli, chopped</p>
        <p>1 large chopped red pepper</p>
        <p>1 cup of chopped shiitake or other mixed mushrooms</p>
        <p>black pepper</p>
        <p>1 tbsp sugar</p>
        <p>lime wedges, dill, coriander, chilli</p>
    </>
);

const Method = () => (
    <>
        <li>In large 2-3 litre pot, boil onion in 2 litres of water until the onion is soft/soggy. Remove the onion</li>
        <li>Add vegetables, chopped chicken and pilchards and cook until chicken and vegetables are cooked</li>
        <li>Crush all aromats in pestle and mortar until smooth or whiz in blender to make a curry paste</li>
        <li>Heat coconut milk in pan until warm to hot.  Add the curry paste </li>
        <li>Tip coconut mixture into water and chicken.  Add the sugar, lime juice, fish sauce, pepper and noodles.  Simmer gently until noodles are cooked</li>
        <li>Whilst simmering, add eggs, stirring to make ribbony egg trails</li>
        <li>Garnish with fresh chopped red chilli, dill, coriander and a wedge of lime</li>
        <li>Eat with a tissue; your nose will run!</li>
    </>
);

export { Ingredients, Method, categories, Blurb };