
import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
<>
    <p>Mum's recipe; I don't think I have ever made these either</p>
</>
);

const Ingredients = () => (
<>
    <p>6 oz cold chopped butter</p>
    <p>2 oz sugar</p>
    <p>8 oz SR flour</p>
    <p>2 oz custard powder</p>
    <p>1 tbsp milk</p>
</>
);

const Method = () => (
    <>
        <li>Set oven to 160ºc</li>
        <li>Grease or line biscuit trays with baking paper</li>
        <li>Mix all ingredients using the flat beater/dough hook style attachment until come together and form a dough</li>
        <li>Roll large teaspoons of mix into a ball and press down with a fork on the top</li>
        <li>Bake in slow oven for approximately 15 minutes; dont really need to brown these</li>
        <li>Join with icing once they are completely cooled</li>
        <li><strong>For Icing</strong>: With the electric beater, mix 110g icing sugar with 80g softened butter until light and fluffy</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
