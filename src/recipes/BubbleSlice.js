import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
    <>
        <p>No bake recipe, this one.  This is kind of like a home made Kellogs ad; sorry about that. Credit Ral Cogs.</p>
    </>
);

const Ingredients = () => (
    <>
        <p>50g chopped butter</p>
        <p>100g white marshmallows</p>
        <p>½ cup crunchy peanut butter</p>
        <p>3 cups of rice bubbles</p>
    </>
);

const Method = () => (
    <>
        <li>Line a slice tray with baking paper</li>
        <li>Melt everything together except bubbles</li>
        <li>Stir in bubbles</li>
        <li>Spread into prepared slice tray</li>
        <li>You can top with combined melted peanut butter (¼ cup) and choc bits (1 cup)</li>
        <li>Cover and put in the fridge to set - at least 4 hours</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
