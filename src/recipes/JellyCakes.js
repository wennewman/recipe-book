import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
<>
    <p>Mum's recipe.  No idea if they are as good as Georgies</p>
</>
);

const Ingredients = () => (
<>
    <p>2 oz butter</p>
    <p>2 oz sugar</p>
    <p>4 oz SR flour</p>
    <p>Pinch of salt</p>
    <p>1 tsp vanilla extract</p>
    <p>2 tbsp milk</p>
    <p>1 egg</p>
    <p>1 cup dessicated coconut</p>
    <p>1 packet of red jelly made to instructions on pack</p>
</>
);

const Method = () => (
    <>
        <li>Heat oven to moderate 180ºc</li>
        <li>Make up the jelly according to instructions and put in the fridge</li>
        <li>Grease and line patty pans or small muffin trays</li>
        <li>Beat butter and sugar with electric mixer until creamy</li>
        <li>Add egg and mix well</li>
        <li>Add sifted flour and milk alternately until well combined</li>
        <li>Spoon mix into patty things and bake in moderate oven for 12-15 minutes</li>
        <li>Remove the cakes and cool on rack</li>
        <li>When jelly is cold but not set, dip the cooled cakes in jelly then roll in coconut</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
