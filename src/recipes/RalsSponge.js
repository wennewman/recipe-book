import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
<>
<p>One day I aspire to bake like my dear Ral. Sponges are not my best work. In fact the only way I can have them not sink in the middle, is by not lining the tin, and hanging them upside down to cool; I saw this trick on a bon appétit video from the test kitchen. Made perfect sense to me given that panettone are cooled like this.</p>
</>
);

const Ingredients = () => (
    <>
        <p>4 separated eggs</p>
        <p>pinch of salt</p>
        <p>2 heaped tsps baking powder</p>
        <p>heaped cup of cornflour</p>
    </>
);

const Method = () => (
    <>
        <li>Set the oven to 190ºc</li>
        <li>Get your two sponge sandwich tins or one round high aluminium cake pan</li>
        <li>Beat eggwhites and salt until stiff</li>
        <li>Add castor sugar a little at a time, beating until you cant feel the grains of sugar when feeling between your fingers</li>
        <li>Add egg yolks one at a time</li>
        <li>Carefully fold in sifted flour and baking powder</li>
        <li>Pour into two tins or your larger single tin and bake for 20-22 minutes.  Don't open the oven while baking.  A skewer should come out clean when they are cooked.</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
