import React from 'react'

const categories = ['lunch', 'brunch', 'afternoon tea'];

const Blurb = () => ( 
<>
    <p>Sanga of dreams; if you eat gluten, dairy and love smoked salmon.  Otherwise... perhaps not.</p>
</>
);

const Ingredients = () => (
    <>   
        <p>250g Cream cheese</p>
        <p>Chopped fresh dill</p>
        <p>Zest and juice of a lemon</p>
        <p>250g smoked salmon</p>
        <p>Whatever gluten or gluten free carrier you desire - I love rye bread for this one or a good sour dough; toast if you prefer or your bread is not fresh</p>
        <p>1 Lebanese cucumber</p>
        <p>1 red onion</p>
        <p>Salt and pepper</p>
    </>
);

const Method = () => (
    <>
        <li>Finely slice onion into rings and place in small bowl.  Cover with Lemon juice, salt and pepper.  Let it sit for at least 5 minutes</li>
        <li>Mix cream cheese and lemon juice (can put in blender if you want)</li>
        <li>Add zest and finely chopped fresh dill - to your taste, salt and pepper</li>
        <li>Toast your bread if you want, then remove the crusts</li>
        <li>Place your bread/toast on a baking sheet and cover with baking paper</li>
        <li>Spread the bread with the cream cheese mixture</li>
        <li>Add a layer of salmon</li>
        <li>Add a layer of cucumber sliced with veggie peeler</li>
        <li>Drain the onion and pat dry with a paper towel.  Add a layer of onion to the top of the cucumber</li>
        <li>Repeat the process until you are all out of junk making the top layer Salmon.  </li>
        <li>Wrap with cling film and press down and refridgerate until you are ready to eat</li>
        <li>Add a curl of cucumber and a couple of onion rings for garnish, and a few fresh sprigs of dill</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
