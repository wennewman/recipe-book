import React from 'react'

const categories = ['dinner'];

const Blurb = () => (
<>
    <p>Two greedy Italians is the credit for this recipe but as I use it all the time, I have added it here.</p> 
</>
);

const Ingredients = () => (
<>
    <p>200g Italian OO flour</p>
    <p>220g full fat ricotta cheese</p>
    <p>3 egg yolks</p>
    <p>30g grated parmesan</p>
    <p>Fresh grated nutmeg</p>
    <p>Salt and pepper</p>
</>
);

const Method = () => (
    <>

    <li>In a large bowl, mix the flour, ricotta, egg yolks, Parmesan, nutmeg and a pinch of salt and pepper together to form a soft, moist dough</li>
    <li>Place on a floured work surface and knead for 3–5 minutes </li>
    <li>With your hands, roll the dough into a long, thin sausage shape and then cut it at right angles into rectangular shapes about 2cm long </li>
    <li>Bring a large saucepan of lightly salted water to the boil and add the dumplings. Wait until they rise to the surface again, then lower the heat and simmer for a further 2 minutes </li>
    <li>Drain the dumplings with a slotted spoon and add to your fav pasta sauce; don't worry if there is some pasta water coming along for the ride </li>
    <li>Serve immediately with buckets of grated parmesan and some fresh basil</li>
    </>
);
const RecipeImage = () => (
    <>
        <img src="../images/ricottaGnocchi.png" alt="Ricotta dumplings" />
    </>
);

export { Ingredients, Method, categories, Blurb, RecipeImage };
