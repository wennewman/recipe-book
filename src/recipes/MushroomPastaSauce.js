import React from 'react'

const categories = ['dinner', 'gluten free'];

const Blurb = () => (
<>
    <p>My goto pasta sauce for Genarro's gnocchi.  Best if you have home made stock in the fridge.</p>
</>
);

const Ingredients = () => (
<>
    <p>Olive oil</p>
    <p>2 garlic cloves</p>
    <p>1 tbsp chopped fresh parsley</p>
    <p>1 tsp chilli flakes (optional)</p>
    <p>1 onion</p>
    <p>2 -3 bacon rashers</p>
    <p>1 punnet of mushrooms</p>
    <p>½ cup chicken stock</p>
    <p>1 fresh tomato (okay if you don't have one but probably don't use tinned toms)</p>
    <p>2 sprigs of fresh thyme</p>
    <p>Salt and pepper</p>
    <p>Splash of cream (optional)</p>
</>
);

const Method = () => (
    <>
        <li>Heat heavy based fry pan and add a big splash of oil</li>
        <li>Finely chop onion and garlic and add to heated oil. Fry for a minute or two on medium-high heat</li>
        <li>Add chopped bacon, mushrooms and tomato if using, and fry until mushrooms are cooked down and juice of tomato has evaporated</li>
        <li>Add fresh parsley, chilli flakes (if using) and stock and cook down to reduce to a saucey consistency (approx 5 minutes)</li>
        <li>You can finish there, or add a splash of cream if you like</li> 
        <li>Serve with your fav pasta and load with parmesan</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
