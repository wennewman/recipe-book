import React from "react"
const categories = ['dinner', 'gluten free'];

const Blurb = () => (
    <>
        <p>You either love it or hate it, but if you love it... here 'tis. Mum used to always boil the snags before she used them.  That is too much pot washing for my liking, just chop them up and fry them until almost cooked instead.  This adds umami to the base of the pan also; use the same pan for the snag cooking as the whole dish - just take them out once they are browned.</p>
        <p>Use your fav GF snags and soy sauce to make it GF.</p>
    </>
)

const Ingredients = () => (
    <>
        <p>2 tbsp olive oil</p>
        <p>1 chopped onion</p>
        <p>2 garlic cloves</p>
        <p>2 chopped or grated green apples</p>
        <p>1 large chopped carrot</p>
        <p>2 chopped celery stalks</p>
        <p>2 chopped tomatoes</p>
        <p>1 tbsp curry powder</p>
        <p>1 tsp salt</p>
        <p>Black pepper to taste</p>
        <p>1 tbsp soy sauce</p>
        <p>1 tsp sugar</p>
        <p>500g chopped butcher's sausages or chipolatas</p>
        <p>1 cup of stock or water</p>
    </>
);

const Method = () => (
    <>
        <li>In a large pan, fry bite sized chopped sausages in oil until almost cooked. Set aside in a bowl</li>
        <li>In the same pan, add a little oil if needed and fry onion, garlic, celery, salt, sugar, pepper and curry powder until onion and carrots have softened</li>
        <li>Add apple and tomatoes and cook for 5 minutes</li>
        <li>Return the cooked, chopped sausages to the pan and add the remaining ingredients.  Put the lid on the pan and simmer until carrots and other vegetables are cooked through (10-15 minutes) </li>
        <li>Serve with mash or rice and vegetables</li>
    </>
);

export { Ingredients, Method, categories, Blurb};