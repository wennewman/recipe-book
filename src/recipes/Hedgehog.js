import React from 'react'

const categories = ['afternoon tea', 'no bake'];

const Blurb = () => (
<>
    <p>This is mum's recipe.  I've not verified this one for years.  You can make the whole thing in a large saucepan.</p>
</>
);

const Ingredients = () => (
<>
    <p>¼ lb butter</p>
    <p>½ lb Marie biscuits</p>
    <p>¼ cup sugar</p>
    <p>2 tbsp cocoa</p>
    <p>1 lightly beaten egg</p>
    <p>½ cup chopped walnuts</p>
</>
);

const Method = () => (
    <>
        <li>In a large saucepan, melt butter, sugar and cocoa</li>
        <li>Stir over the heat until the sugar slightly dissolves</li>
        <li>Take off the heat and allow to cool</li>
        <li>Smash the bikkies to your liking in a bag with rolling pin or pulse in the food processor</li>
        <li>Once the butter mixture is cooled, add the egg and combine until it goes thick</li>
        <li>Stir in the crushed biscuits then press into a slice tray and pop in the fridge to set</li>
        <li>You can ice with chocolate icing if you like and sprinkle with dessicated coconut</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
