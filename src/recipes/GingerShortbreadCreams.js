import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
<>
    <p>Mum's recipe from Aunty Rube.  She used to use the bikky forcer for these.</p>
</>
);

const Ingredients = () => (
<>
    <p>½lb butter</p>
    <p>2½ cups flour</p>
    <p>1 cup sugar</p>
    <p>2 tsp ginger powder</p>
    <p>1 egg</p>
    <p>1 tbsp golden syrup</p>
    <p>1 tsp bi carb soda</p>
</>
);

const Method = () => (
    <>
        <li>Set oven to moderate 180ºc</li>
        <li>Grease biscuit trays or cover with baking paper</li>
        <li>Cream butter and sugar with electric mixer</li>
        <li>Add egg and syrup and beat until combined</li>
        <li>Add sifted dry ingredients and beat until it comes together into a lump of bikky dough</li>
        <li>Use a biscuit forcer or roll into balls and press down with a fork or the back of a wet teaspoon</li>
        <li>Bake for 12-15 minutes</li>
        <li>These can be joined together with lemon or vanilla icing/butter cream</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
