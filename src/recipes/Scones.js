import React from 'react'

const categories = ['afternoon tea'];

const Blurb = () => (
    <>
        <p>Credit Ral Cogs</p>
    </>
);

const Ingredients = () => (
    <>
        <p>4 cups SR flour</p>
        <p>300ml cream</p>
        <p>¼ tsp salt</p>
        <p>3/4 cup milk</p>
    </>
);

const Method = () => (
    <>
        <li>Set oven to 220ºc</li>
        <li>Sift flour and salt</li>
        <li>Mix cream and milk together and add into sifted flour</li>
        <li>Combine with a knife until messy dough and then knead lightly on floured bench.  Do not over work the dough - Add a little extra milk if too dry</li>
        <li>Roll out to scone height and cut out with knife or scone cutter</li>
        <li>Cook in hot oven for 12-14 minutes</li>
        <li>Serve hot with all the butter and jam and cream and anything else you want</li>
    </>
);

export { Ingredients, Method, categories, Blurb };
