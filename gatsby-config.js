module.exports = {
  siteMetadata: {
    title: `Wen's recipes`,
    description: `🛫🛫🛬🛬🛩🛩🚆🚄🚄🚉🚉🚂`,
    author: `wen and chloe`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `wens-recipes`,
        short_name: `wens-recipes`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#38A21B`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
